//primitive vs reference types

//Primitive
let num1=6;
let num2=num1;
console.log(`value of num 1 is ${num1}`);
console.log(`value of num 2 is ${num2}`);
num1++;
console.log('After increment'); 
console.log(`value of num 1 is ${num1}`);
console.log(`value of num 2 is ${num2}`);

//Reference Type
let arr1=["item1","item2"];
let arr2=arr1;
console.log(`value of array 1 is ${arr1}`);
console.log(`value of array 2 is ${arr2}`);
arr1.push("item3");
console.log('After increment'); 
console.log(`value of array 1 is ${arr1}`);
console.log(`value of array 2 is ${arr2}`);




//How to Clone Array   
let arr_1=["item 1","item 2"];
let arr_2 =arr_1.slice(0).concat(arr_1);
let arr_3 = [].concat(arr_1); 

//spread  array
let arr_4=[...arr_1,"ayssh","manas"];
//Objects
//Objects are reference type
//object store key value pairs
//Object dont have index

//object creation

const person={name:"Ayush",age:22,hobbies:["chess","crickt","code"]};
console.log(person);

//access particular item
console.log(person.age);
console.log(person.hobbies);

//adding particular keyValue Pair
person.gender="Male";
console.log(person);

//iterate objects
//for in loop
//Object.Keys
 
for(let key in person){
    // console.log(`${key} : ${person[key]}`);
    console.log(key," : " ,person[key]);
}

console.log(typeof (Object.keys(person)));
const val = Array.isArray((Object.keys(person)));
console.log(val);

for(let key of Object.keys(person)){
    console.log(person[key]);
}
// computed properties

const key1 = "objkey1";
const key2 = "objkey2";

const value1 = "myvalue1";
const value2 = "myvalue2";

// const obj = {
//     objkey1 : "myvalue1",
//     objkey2 : "myvalue2",
// }

// const obj = {
//     [key1] : value1,
//     [key2] : value2
// }

const obj = {};

obj[key1] = value1;
obj[key2] = value2;
console.log(obj);


//Spread Operator
 //object Destructuring
 const band = {
    bandName: "led zepplin",
    famousSong: "stairway to heaven",
    year: 1968,
    anotherFamousSong: "kashmir",
  };
  
  let { bandName:B_name, famousSong, ...restProps } = band;
  console.log(B_name);
  console.log(restProps);

  // objects inside array 
// very useful in real world applications

const users = [
    {userId: 1,firstName: 'Ayush', gender: 'male'},
    {userId: 2,firstName: 'Manas', gender: 'male'},
    {userId: 3,firstName: 'Naphade', gender: 'male'},
]
for(let user of users){
    console.log(user);
}


//Nested Destructure 
const users1 = [
    {userId: 1,firstName: 'harshit', gender: 'mddde'},
    {userId: 2,firstName: 'mohit', gender: 'male'},
    {userId: 3,firstName: 'nitish', gender: 'mae'},
]

const [{firstName: user1firstName, userId}, , {gender: user3gender}] = users1;
console.log(user1firstName);
console.log(userId);
console.log(user3gender);
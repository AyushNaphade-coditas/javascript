"use strict";
//use strict tell us which mistake we have made while declring variable
//without "var" we can declare variable
//variable store information
console.log("Variable");

//variable declaration
var var1="Ayush Naphade"
console.log(var1);

//change value
var1="Mansi Patil";
console.log(var1);



//rules for variable declaration
//variable name cant start with number
// var 1name(not valid)
var value1=2;

//you can use underscore and dolar
//cant use space 
// var _firstname/var firstname_  (valid)
var _name ="JavaScript";
var name_ ="language";
var firstName ="Ayush";//camel case writing
var first_name="Mansi";//snake case 

//convention
//Start ith smal letter and use camelcase



//We can create variable using "let keyword"

let last_name="Naphade";
//always prefer let to creat variable
console.log(last_name);



//Declare Constant
const pi=3.14;
console.log(pi);

//--------------------------------------------------------------------
//STRING METHOD
//--------------------------------------------------------------------

//String Indexing

let string_Name="Anirudha"
// a   n   i  r  u  d  h  a
// 0   1   2  3  4  5  6  7
console.log(string_Name[2]);
console.log(string_Name.length);


//last index :length-1;




//trim()-remove extra spaces from both side 
let variable_1 ="   AyUsH    ";
console.log(variable_1.length);
variable_1=variable_1.trim();
console.log(variable_1);
console.log(variable_1.length);

//toUpperCase()
console.log(variable_1.toUpperCase());

//toLowerCase()
console.log(variable_1.toLocaleLowerCase());

//slice
//start index- include
//last index -exclude
console.log(variable_1.slice(0,3));

//Data Types(premitive)
//string ex "ayuh"
//number  2,6,4.5
//boolean
//undefined
//null
//BigInt
//Symbol
let age=22;
let name1="Ayush";
console.log(typeof age);
console.log(typeof name1);


//convert  number to string
console.log(typeof (age=age+""));

let str=+"34";
console.log(typeof str);

let age1="22";
age1=Number(age1);
console.log(typeof age1);


//------------------------
//String Concatenation
//------------------------
//+ sign is used to concate strings



//template string
let age2=22;
let _name1="Ayush";
let about="My name is " +_name1 +" "+ "My age is "+age2 ;
console.log(about);

let aboutme=`my name is ${_name1} and my age is ${age2}`;
console.log(aboutme);

//cant add bigint with other operation
let a=BigInt(123);
let b= 123n;
console.log(a+b);


//booleans and comparison operator <,>,==,>=,<=

//== vs ==      != vs !==
// == will check only value
// === will check data type and value

// falsy values
//false
//""
//null
//undefined
//0

let value_falsy="";
if(value_falsy){
    console.log(value_falsy);
}
else{
    console.log("Empty value");
}

// truely
//1,-1
//"abc"

//ternary operator
let c=10;
let drink= age>=8 ? "coffee":"milk";
console.log(drink);



//if else condition
let age3=15;
if(age3%2===0){
    console.log("Even");
}else{
    console.log("0dd");
}


//&& and || operatior
let firstName6 = "arshit";
let age5 = 16;
if(firstName6[0] === "H" && age5>10){
    console.log("Name starts with H and above 18");
}else{
    console.log("inside else");
}

if(firstName[0] === "H" || age5>10){
    console.log("Name starts with H or above 18");
}else{
    console.log("inside else");
}




//nested if else

let winningNmber=12;
let userGuess=+prompt("Guess a Number");


//if elseif elseif
if(true){

}
else if(flase){

}
else if(true){

}
else{

}

//switch cases
let choice=+prompt("Enter Number");
switch(choice){
    case 0:
        break;
    case 1:
        break;
    case 2:
        break;
    case 3:
        break;
    default:       

}

//While Loop

let i=0;
while(i<=13){
    console.log(i);
    i++;
}

let j=10;
let k=0;
let l=1;
while(l<=j){
    k+=l;
    l++;
}
console.log(k);


//for loop

for(let i=0;i<=9;i++){
    console.log(i);
} 

//break 
for(let i=0;i<=9;i++){
    if(i==4){
        break;
    }
    console.log(i);
}

//continue
for(let i=0;i<=9;i++){
    if(i==4){
        continue;
    }
    console.log(i);
}

//do while
let z=0;
do{
    console.log(z);
    z++;
}
while(z<=9)


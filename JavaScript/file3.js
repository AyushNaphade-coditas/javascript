//Introduction TO Array.
//Reference Type

//how to create array
//arrays are mutables

let fruits=[".net","html","css","javascript"];
console.log(fruits);

//unshift will addd element at start
fruits.unshift("Mansi");
console.log(fruits)

//shift will remove element at start
fruits.shift();
console.log(fruits.shift());


//push add new element is array at last
fruits.push("Manas");

//pop:will remove last element
fruits.pop();
console.log(fruits.pop());//pop will remove as well as print element

//push/pop is faster than unshift/shift

//replace elemen in particular index
fruits[1]="Ayush";
console.log(fruits);


let numbers=[1,2,3,4,5];
let mixed=[1,2,3,".net",null,undefined];
console.log(typeof fruits);

console.log(fruits[1]);
console.log(numbers);
console.log(mixed);

//obj literal
let obj={};
Console.log(Array.isArray(obj));
Console.log(Array.isArray(fruits));


//for loop in array
let fruits_2=[];
let fruits=["banana","apple","mango","chiku"];
for(i=0;i<fruits.length;i++){
   fruits_2.push(fruits[i].toUpperCase());
}
console.log(fruits_2);


//use const for creating array
const fruit=["ayush","manas","mansi"];

let j=0;
while(j<fruit.length){
    console.log(fruit[j]);
    j++;
}

// fruit.push("manas");
// console.log(fruit);
//similar t foreach in C#
for(let fr of fruit){
    console.log(fr);
}

//Array Destructuring

const myArray=["value1","value2"];
//normal method
let myarr1=myArray[0];
let myarr2=myArray[1];

//Array Destructuring
let[myrr,myrr2]=myArray;




// important array methods 

const numbers = [4,2,5,8];


const users = [
    {firstName: "harshit", age: 23},
    {firstName: "mohit", age: 21},
    {firstName: "nitish", age: 22},
    {firstName: "garima", age: 20},
]

numbers.forEach(function(number,index){
    console.log(`index is ${index} number is ${number}`);
});


users.forEach(function(user){
    console.log(user.firstName);
});
